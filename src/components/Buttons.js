import React from 'react';
import composeBranding from '../utils/allowBranding';

const Buttons = (props) => (
	<div className="Buttons">
		{props.children}
	</div>
);

export default composeBranding('Beezy.Buttons', Buttons);