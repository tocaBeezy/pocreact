import React from 'react';
import composeBranding from '../utils/allowBranding';


const Display = (props) => {
	var string = props.data.join('');
	return (
		<div className="Display">
			{string}
		</div>
	);
};

export default composeBranding('Beezy.Display', Display);