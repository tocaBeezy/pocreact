import React from 'react';
import brandingRegistry from './brandingRegistry';


const composeBranding = (displayName, Beezycomponent) => {

	function addProps(dest, Obj){
		for (var name in Obj) {
			if (Obj.hasOwnProperty(name) ){
				dest.prototype[name] = Obj[name];
			}
		}
	}

	function getProps (obj) {
		var props = {};
		for (var name in obj) {
			if (obj.hasOwnProperty(name) ){
				props.override = true;
				props[name] = obj[name];
			}
		}
		return props;
	}

	function getWrapperComponent () {
			const BrandingRegistered = brandingRegistry.get(displayName);
			const WrapperComponent = (BrandingRegistered) ? BrandingRegistered : Beezycomponent;
			WrapperComponent.displayName = displayName;
			if (BrandingRegistered) {
				WrapperComponent.isBrandered = true;
				getWrapperComponent =  (props, context, updater) => {
					return WrapperComponent;
				}
			}
			return WrapperComponent;
	}

	function returnComponent (props, context, updater) {
		const proto = getProps(returnComponent.overrides);
		let WrapperComponent = getWrapperComponent();
		if (proto.override) {
			addProps(WrapperComponent, proto);
			returnComponent = (props, context, updater) => {
				return <WrapperComponent {...props}>{props.children}</WrapperComponent>;
			}
		}
		return <WrapperComponent {...props}>{props.children}</WrapperComponent>;
	};

	returnComponent.basePrototype = Beezycomponent.prototype;
	returnComponent.overrides = {};

	return returnComponent;
};



export class BaseComponent extends React.Component {
	constructor(props, displayName) {
		super(props);
		let WrapperComponent = this;
		const BrandingRegistered = brandingRegistry.get(displayName);
		if (BrandingRegistered) {
			WrapperComponent = new BrandingRegistered(props, this.context, this.updater);
		}
		return WrapperComponent;
	}
}

export default composeBranding;
