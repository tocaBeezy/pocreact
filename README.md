# Composition vs Inheritance
[React Docs: Composition vs inhertance](https://facebook.github.io/react/docs/composition-vs-inheritance.html)

React has a powerful composition model, and we recommend using composition instead of inheritance to reuse code between components.

In this section, we will consider a few problems where developers new to React often reach for inheritance, and show how we can solve them with composition.

## So What About Inheritance?
At Facebook, we use React in thousands of components, and we haven't found any use cases where we would recommend creating component inheritance hierarchies.

Props and composition give you all the flexibility you need to customize a component's look and behavior in an explicit and safe way. Remember that components may accept arbitrary props, including primitive values, React elements, or functions.

If you want to reuse non-UI functionality between components, we suggest extracting it into a separate JavaScript module. The components may import it and use that function, object, or a class, without extending it.


# Higher-Order Components

A higher-order component (HOC) is an advanced technique in React for reusing component logic. HOCs are not part of the React API, per se. They are a pattern that emerges from React's compositional nature.

Concretely, a **higher-order component is a function that takes a component and returns a new component**

```js
const EnhancedComponent = higherOrderComponent(WrappedComponent);
```

Whereas a component transforms props into UI, a higher-order component transforms a component into another component.

HOCs are common in third-party React libraries, such as **Redux's** ``connect`` and **Relay's** ``createContainer``.

	Note
	We previously recommended mixins as a way to handle cross-cutting concerns. We've since realized that mixins create more trouble than they are worth. Read more  about why we've moved away from mixins and how you can transition your existing components.

[React Docs: High-Order Components](https://facebook.github.io/react/docs/higher-order-components.html)

## Recomendations of Using HOCs

### Don't Mutate the Original Component. Use Composition.
Resist the temptation to modify a component's prototype (or otherwise mutate it) inside an HOC.

```js
function logProps(InputComponent) {
  InputComponent.prototype.componentWillReceiveProps(nextProps) {
    console.log('Current props: ', this.props);
    console.log('Next props: ', nextProps);
  }
  // The fact that we're returning the original input is a hint that it has
  // been mutated.
  return InputComponent;
}

// EnhancedComponent will log whenever props are received
const EnhancedComponent = logProps(InputComponent);
```

This HOC also won't work with function components, which do not have lifecycle methods.

Mutating HOCs are a leaky abstraction—the consumer must know how they are implemented in order to avoid conflicts with other HOCs.

Instead of mutation, HOCs should use composition, by wrapping the input component in a container component:

```js
function logProps(WrappedComponent) {
  return class extends React.Component {
    componentWillReceiveProps(nextProps) {
      console.log('Current props: ', this.props);
      console.log('Next props: ', nextProps);
    }
    render() {
      // Wraps the input component in a container, without mutating it. Good!
      return <WrappedComponent {...this.props} />;
    }
  }
}
```

## Branding registry

A record is created where the components that the parner registers are stored, the component namespace and its creation function are stored.

```js
brandingRegistry.get(displayName); //'Beezy.Button'
brandingRegistry.add('Beezy.Button', ZFButtonBranding); // where ZFButtonBranding its a create function of branding component
```

## Branding vs Solution

From the visual studio branding solution, we must refer to BrandingRegistry to be able to overwrite components of our catalog.

```js
import ZFButtonBranding from './components/ZFButtonBranding';
import React from 'react';
import ReactDOM from 'react-dom';

define(function(require) {
	'use strict';
	const mappings = window.BeezyApp.mappings;

	return window.BeezyApp.define('poc-react', [
		mappings.product.javascript.poc_react.src.utils.brandingregistry.js,
		mappings.product.javascript.poc_react.src.app.js
		], function(brandingRegistry, App) {

			// Overwrite a BeezyComponent 'Beezy.Button' with ZFButtonBranding
			// component exclusive for ZF Branding
			brandingRegistry.default.add('Beezy.Button', ZFButtonBranding);

			// Extends actual component
			App.default.overrides.sayHello = function() {
				console.log("Hello world for new extend method", this.state);
			};

			// Save original method to use after.
			const calculateOperations = App.default.basePrototype.calculateOperations;

			// Overwrite method
			App.default.overrides.calculateOperations = function () {
				this.sayHello();
				calculateOperations.call(this);
			};

		});
});
```

We can add components with ```brandingRegistry.add```,

We can override methods of the current components by accessing their ```basePrototype```.

To add methods to the current prototype we must use ```overrides```.

## Beezy Components

Whenever we create a component and want it to be accessible for banding, we must use the ```composeBranding```.

Every time we think a component and want it to be accessible to banding, we use the composeBranding function function found in ```utils/allowBranding.js```.

```composeBranding``` allows adding a HOC to our component that the first time it is requested check if that component has been registered by the parner to return it.

Once returned, the creation function of the component will be modified so that it does not re-check and always returns the final component.

```js
import React from 'react';
import composeBranding from '../utils/allowBranding';

export const Button = (props) => (
	<div onClick={props.onClick} className="Button" data-size={props.size} data-value={props.value}>
				{props.label}
	</div>
);

export default composeBranding('Beezy.Button', Button);
```


