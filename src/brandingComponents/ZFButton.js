import React, { Component } from 'react';

/*For testing porpouses, can remove */

class ZFButton extends Component {
   render() {
    const label =  `&#9786;${this.props.label}`;
    return (
      <div onClick={this.props.onClick} className="Button" data-size={this.props.size} data-value={this.props.value}>
      &#9786; {this.props.label}
      </div>
    )
  }
}

export default ZFButton