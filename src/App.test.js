import React from 'react';
import ReactDOM from 'react-dom';
import { mount, shallow } from 'enzyme';
import { expect } from 'chai';
import sinon from 'sinon';

import App from './App';
import Display from './components/Display';
import Buttons from './components/Buttons';
import Button from './components/Button';
import brandingRegistry from './utils/brandingRegistry';
import ZFButton from './brandingComponents/ZFButton.js';

// Test the output
describe('Test the output', () => {
  it('renders without crashing', () => {
    const component = shallow(<App />);
    expect(component.find('App').length).to.be.equal(1);
  });


  it('OverWrite BeezyButton', () => {
    const div = document.createElement('div');
    brandingRegistry.add('Beezy.Button', ZFButton);
    ReactDOM.render(<App />, div);
  });

  it('Contains childrens', () => {
    const component = shallow(<App />);
    expect(component.containsAllMatchingElements([
      <Display />,
      <Buttons />
    ]))
  });
});

describe('Test the react component state', () => {
  it('should start with operations empty list', () => {
    const component = shallow(<App />);
    const child = shallow(component.get(0));
    expect(child.state('operations')).to.be.eql([]);
  });

  it('adds Items to operations list', () => {
    const component = shallow(<App />);
    const child = shallow(component.get(0));

    child.setState({ 'operations' :['5'] });

    expect(child.state('operations')).to.be.eql(['5']);
  });

  it('call method calculateOperations in App', () => {
    // Obtains HOC of App
    const component = shallow(<App />);
    // wrapper of App by enzyme
    const wrapper = shallow(component.get(0));

    wrapper.setState({ 'operations' :['5', '+', '5'] });
    wrapper.instance().calculateOperations();
    expect(wrapper.state('operations')).to.be.eql(['10']);
    wrapper.setState({ 'operations' :['10', '+', '5'] });
  });
});

describe('Test events', () => {
  it("call button click evetn", () => {
    // Obtains HOC of App
    const component = shallow(<App />);
    // wrapper of App by enzyme
    let wrapper = component.get(0);
    // Create spy to method handleclick of App
    const handleClick = sinon.spy();
    // Overwrite original handleclick from constructor function of App
    wrapper.type.prototype.handleClick = handleClick;
    // Instantiate of modified App with spy
    wrapper = shallow(wrapper);
    // enzyme wrapper of Button to simulate Click
    const button = shallow(wrapper.find(Button).get(0));
    button.simulate('click');
    // check the mock
    expect(handleClick).to.have.property('callCount', 1);
  });
});
