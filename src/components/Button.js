import React from 'react';
import composeBranding from '../utils/allowBranding';

export const Button = (props) => (
	<div onClick={props.onClick} className="Button" data-size={props.size} data-value={props.value}>
				{props.label}
	</div>
);

export default composeBranding('Beezy.Button', Button);