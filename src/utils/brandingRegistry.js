const registry = {};

const BrandingRegistry = () => ({
	get: (key) => registry[key],
	add: (key, Component) => { registry[key] = Component; }
});

export default new BrandingRegistry();